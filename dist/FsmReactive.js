"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const EventNotifier_1 = require("@archjs/event-notifier/EventNotifier");
const FSM_1 = require("@archjs/fsm/FSM");
class FsmReactive extends FSM_1.FSM {
    constructor(data) {
        super(data);
        this._EventEmitter = new EventNotifier_1.EventNotifier(['transition', 'next']);
        for (var i in this._transitions) {
            this._EventEmitter.createEvent(this._transitions[i].name);
        }
    }
    next(input) {
        var result = super.next(input);
        this._EventEmitter.trigger('next', result);
        return result;
    }
    transition(input, newState) {
        var result = super.transition(input, newState);
        this._EventEmitter.trigger('transition', result);
        this._EventEmitter.trigger(result.eventName, result);
        return result;
    }
    on(eventName, callback) {
        this._EventEmitter.on(eventName, callback);
    }
    trigger(eventName, data) {
        this._EventEmitter.on(eventName, data);
    }
}
exports.FsmReactive = FsmReactive;
//# sourceMappingURL=FsmReactive.js.map