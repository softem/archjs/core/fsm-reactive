import { IEventNotifier, EventNotifier } from "@archjs/event-notifier/EventNotifier";
import { FSM, FSMData } from "@archjs/fsm/FSM";
import { ITransitionStream } from "@archjs/fsm/ITransitionStream";
declare class FsmReactive extends FSM implements IEventNotifier {
    protected _EventEmitter: EventNotifier;
    constructor(data: FSMData);
    next(input?: any): ITransitionStream;
    transition(input?: any, newState?: string): ITransitionStream;
    on(eventName: string, callback: Function): void;
    trigger(eventName: string, data: any): void;
}
export { FsmReactive };
