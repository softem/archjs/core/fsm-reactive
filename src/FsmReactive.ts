import { IEventNotifier, EventNotifier} from "@archjs/event-notifier/EventNotifier";
import {FSM, FSMData} from "@archjs/fsm/FSM";
import { ITransitionStream } from "@archjs/fsm/ITransitionStream";

class FsmReactive extends FSM implements IEventNotifier{
    protected _EventEmitter:EventNotifier;

    public constructor( data:FSMData ){
        super(data);
        this._EventEmitter = new EventNotifier(['transition', 'next']);
        for( var i in this._transitions ){
            this._EventEmitter.createEvent( this._transitions[i].name );
        }
    }

    public next( input? ){
        var result = super.next( input );
        this._EventEmitter.trigger('next', result );
        return result;
    }

    public transition( input?:any, newState?:string ):ITransitionStream{
        var result = super.transition(input, newState);
        this._EventEmitter.trigger('transition', result );
        this._EventEmitter.trigger( result.eventName, result );
        return result;
    }

    public on( eventName: string, callback: Function ) {
        this._EventEmitter.on( eventName, callback );
    }

    public trigger(eventName: string, data: any) {
        this._EventEmitter.on( eventName, data );
    }

}

export { FsmReactive };
